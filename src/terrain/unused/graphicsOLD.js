/*- Variables -*/
const SNOWFLAKE_COUNT = 3;

bunnySpin = false;
var bunny;
var snowflake_textures = new Array();



/*- Load -*/

// Textures
var loader = new THREE.TextureLoader();
loader.load('img/box.png', onTextureLoaded);

function addSnowflakeTexture(texture){
    snowflake_textures.push(texture);
}

// Objects
var objLoader = new THREE.OBJLoader();
objLoader.load( 'objects/bunny.obj', onBunnyLoaded);



/*- Scene Setup -*/

// Setup three.js WebGL renderer. Note: Antialiasing is a big performance hit.
var renderer = new THREE.WebGLRenderer({antialias: false});
renderer.setPixelRatio(Math.floor(window.devicePixelRatio));

// Append the canvas element created by the renderer to document body element.
document.body.appendChild(renderer.domElement);

// Create a three.js scene.
var scene = new THREE.Scene();

// Create a three.js camera.
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 10000);

// Apply VR headset positional data to camera.
var controls = new THREE.VRControls(camera);

// Apply VR stereo rendering to renderer.
var effect = new THREE.VREffect(renderer);
effect.setSize(window.innerWidth, window.innerHeight);



/*- Objects -*/

// Bunny
function onBunnyLoaded( object ) {
    var material = new THREE.MeshPhongMaterial({
        color: 0xffffff,
        wireframe: false,
        specular: 0x888888,
        shininess: 1
    });
    object.traverse(function(child){
        if(child instanceof THREE.Mesh){
            child.material = material;
        }
    })

    bunnySpin = true;
    bunny = object;

    bunny.position.set(0, -3, -4);

    // scene.add( bunny );
}

// Skybox
var boxWidth = 20;

function onTextureLoaded(texture) {
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(boxWidth, boxWidth);

    var geometry = new THREE.BoxGeometry(boxWidth, boxWidth, boxWidth);
    var material = new THREE.MeshBasicMaterial({
        map: texture,
        color: 0x005500,
        side: THREE.BackSide
    });
var skybox = new THREE.Mesh(geometry, material);
scene.add(skybox);
}



// Terrain
var Terrain_Size = 1;
var Terrain_Position = new THREE.Vector3(-0.5, -0.75, -1.5);

var material = new THREE.MeshPhongMaterial({
    vertexColors: THREE.VertexColors,
    wireframe: false,
    specular: 0x888888,
    shininess: 1
});
// var material = new THREE.MeshStandardMaterial({
//     vertexColors: THREE.VertexColors,
//     wireframe: false,
//     metalness: 0,
//     roughness: 1
// });
var terrain = new THREE.Mesh(geometry, material);

// scale and place
var scale = Terrain_Size / masterGrid.length;
terrain.scale.set(scale, scale, scale);
terrain.position.set(Terrain_Position.x, Terrain_Position.y, Terrain_Position.z);
scene.add(terrain);

var geometry = new THREE.SphereGeometry(0.2);
var material = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    side: THREE.BackSide
});
var ptt = new THREE.Mesh(geometry, material);
ptt.position.set(Terrain_Position.x, Terrain_Position.y, Terrain_Position.z);
//scene.add(ptt);

/*- Lighting -*/

// Sun
var sun = new THREE.DirectionalLight(0x666666, 1.5);
sun.position.set(-0.4, 1, -0.2);
scene.add(sun);

// Ambient
var ambient = new THREE.AmbientLight(0x222222);
scene.add(ambient);


/*- Particles -*/

// Snow
var particleCount = 800;

// One single geometry used to house all of the vertices we want to render on
var particles = new THREE.Geometry();

// Array of velocity vectors for snow
var particleVelocity = new Array();

// Add particle vertices
for (var p = 0; p < particleCount; p++){

    // randomly generate positions
    var x = Math.random() * Terrain_Size;
    var y = Math.random() * Terrain_Size;
    var z = Math.random() * Terrain_Size; 

    // create a vertex
    var particle = new THREE.Vector3(x, y, z);

    // assign a velocity to that particle
    var velocity = new THREE.Vector3(Math.random() * 0.0001, 0.0002, Math.random() * 0.0001 );

    // add to the particle geometry
    particles.vertices.push(particle);

    // add velocity to particle velocity array
    particleVelocity.push(velocity);
}

// Create a material for the snow
var snowMaterial = new THREE.PointsMaterial({
    color: 0xdddddd,
    size: 0.01,
    map: new THREE.TextureLoader().load("img/snowflakes_2.png"),
    blending: THREE.AdditiveBlending,
    transparent: true
});

particleSystem = new THREE.Points(particles, snowMaterial);
particleSystem.position.set(Terrain_Position.x, Terrain_Position.y, Terrain_Position.z);

scene.add(particleSystem);


// Make snow fall and flutter
function animateSnow(delta){

    // adjust each vertex
    for(var i = 0; i <  particleSystem.geometry.vertices.length; i++){
        var vertex = particleSystem.geometry.vertices[i];
        if(vertex.y <  -(Terrain_Size/5) || vertex.x >  Terrain_Size || vertex.x < 0 || vertex.z >  Terrain_Size || vertex.z < 0){

            vertex.y = Terrain_Size; //Math.random() * 10 - 5;
            vertex.x = Math.random() * Terrain_Size;
            vertex.z = Math.random() * Terrain_Size;
        }
        vertex.y = vertex.y - (0.0002 * delta)
        vertex.x += particleVelocity[i].x * delta;
        vertex.z += particleVelocity[i].z * delta;
    }

    // Mark the geometry for updating
    particleSystem.geometry.verticesNeedUpdate = true;
}
ptt.position.set(particleSystem.position.x, particleSystem.position.y, particleSystem.position.z);

/*- Render -*/

// Request animation frame loop function
var lastRender = 0;

function animate(timestamp) {
    var delta = Math.min(timestamp - lastRender, 500);

    animateSnow(delta);

    lastRender = timestamp;

    if(rotateTerrain){
        // Apply rotation to terrain mesh aroun center
        terrain.translateX(Terrain_Size/2).translateZ(Terrain_Size/2);
        terrain.rotation.y += delta * 0.0001;// .0006
        terrain.translateX(-Terrain_Size/2).translateZ(-Terrain_Size/2);

        particleSystem.translateX(Terrain_Size/2).translateZ(Terrain_Size/2);
        particleSystem.rotation.y += delta * 0.0001;
        particleSystem.translateX(-Terrain_Size/2).translateZ(-Terrain_Size/2);
    }

    if(bunnySpin){
        //bunny.rotation.y += delta * 0.0006;
    }

    // Update VR headset position and apply to camera.
    controls.update();
    
    camera.zoom = zoom;
    camera.updateProjectionMatrix();

    // Render the scene.
    effect.render(scene, camera);

    // Keep looping.
    vrDisplay.requestAnimationFrame(animate);
}




/*- Handle Events -*/

// Get the VRDisplay and save it for later.
var vrDisplay = null;
navigator.getVRDisplays().then(function(displays) {
    if (displays.length > 0) {
        vrDisplay = displays[0];

        // Kick off the render loop.
        vrDisplay.requestAnimationFrame(animate);
    }
});

function onResize() {
    console.log('Resizing to %s x %s.', window.innerWidth, window.innerHeight);
    effect.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
}

function onVRDisplayPresentChange() {
    console.log('onVRDisplayPresentChange');
    onResize();
}

// Resize the WebGL canvas when we resize and also when we change modes.
window.addEventListener('resize', onResize);
window.addEventListener('vrdisplaypresentchange', onVRDisplayPresentChange);

// Button click handlers.
document.querySelector('button#fullscreen').addEventListener('click', function() {
    enterFullscreen(renderer.domElement);
});
document.querySelector('button#vr').addEventListener('click', function() {
    vrDisplay.requestPresent([{source: renderer.domElement}]);
});
document.querySelector('button#reset').addEventListener('click', function() {
    vrDisplay.resetPose();
});

function enterFullscreen (el) {
    if (el.requestFullscreen) {
        el.requestFullscreen();
    } else if (el.mozRequestFullScreen) {
        el.mozRequestFullScreen();
    } else if (el.webkitRequestFullscreen) {
        el.webkitRequestFullscreen();
    } else if (el.msRequestFullscreen) {
        el.msRequestFullscreen();
    }
}


function makeTextSprite( message, parameters )
{
	if ( parameters === undefined ) parameters = {};
	
	var fontface = parameters.hasOwnProperty("fontface") ? 
		parameters["fontface"] : "Arial";
	
	var fontsize = parameters.hasOwnProperty("fontsize") ? 
		parameters["fontsize"] : 18;
	
	var borderThickness = parameters.hasOwnProperty("borderThickness") ? 
		parameters["borderThickness"] : 4;
	
	var borderColor = parameters.hasOwnProperty("borderColor") ?
		parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };
	
	var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
		parameters["backgroundColor"] : { r:255, g:255, b:255, a:1.0 };
	//var spriteAlignment = THREE.SpriteAlignment.topLeft;
		
	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');
	context.font = "Bold " + fontsize + "px " + fontface;
    
	// get size data (height depends only on font size)
	var metrics = context.measureText( message );
	var textWidth = metrics.width;
	
	// background color
	context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
								  + backgroundColor.b + "," + backgroundColor.a + ")";
	// border color
	context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
								  + borderColor.b + "," + borderColor.a + ")";
	context.lineWidth = borderThickness;
	//roundRect(context, borderThickness/2, borderThickness/2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
	// 1.4 is extra height factor for text below baseline: g,j,p,q.
	
	// text color
	context.fillStyle = "rgba(0, 0, 0, 1.0)";
	context.fillText( message, borderThickness, fontsize + borderThickness);
	
	// canvas contents will be used for a texture
	var texture = new THREE.Texture(canvas) 
	texture.needsUpdate = true;
	var spriteMaterial = new THREE.SpriteMaterial( 
		{ map: texture, useScreenCoordinates: false } );
	var sprite = new THREE.Sprite( spriteMaterial );
	sprite.scale.set(100,50,1.0);
	return sprite;	
}