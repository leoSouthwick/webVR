import { calcGridSize, createGrid, prepareData } from './terrain.js';
import { randomNormal } from './randomNormal.js';
import * as THREE from 'three';
// var SEED = 0;
/* enter here on page reload */
export default function (material) {

    /* Config stuff */
    // WebVRConfig = {
    //     BUFFER_SCALE: 1.0,
    // };
    // document.addEventListener('touchmove', function(e) {
    //     e.preventDefault();
    // });

    var mainBefore = Date.now();

    /* smoothness constant */
    var H = 1.8;

    /* level of detail 9 reasonably well*/
    var detail = 7;

    /* number of grids stitched together (side length) */
    var numGridsSquared = 2

    var before = Date.now();
    var masterGrid;
    var gridSize = calcGridSize(detail);

    // if no grids are to be stitched together then use a preGrid from user input
    // TODO: user input for pregrid from google sheets
    if (numGridsSquared == 0) {
        var preGrid = [
            [randomNormal(10, gridSize / 4), randomNormal(10, gridSize / 4)],
            [randomNormal(10, gridSize / 4), randomNormal(10, gridSize / 4)]
        ];
        masterGrid = createGrid(detail, gridSize, null, preGrid, H);

    } else {

        // create many grids to make the landscape more interesting
        var grids = [];
        for (var i = 0; i < numGridsSquared; i++) {
            grids[i] = [];
            for (var j = 0; j < numGridsSquared; j++) {
                // create neighbors, only south and west will exist due to the
                // order these are being created.
                var neighbors = {
                    south: (j - 1 >= 0) ? grids[i][j - 1] : null, // the condition is to not go out of bounds on the array
                    west: (i - 1 >= 0) ? grids[i - 1][j] : null // the condition is to not go out of bounds on the array
                };
                grids[i][j] = createGrid(detail, gridSize, neighbors, null, H);
            }
        }

        // put all the grids together into the master grid
        masterGrid = [];
        for (var i = 0; i < numGridsSquared; i++) {
            for (var j = 0; j < gridSize; j++) {
                // each grid's edges are equal to their neighbors so skip it
                if (j == gridSize - 1 && i != numGridsSquared - 1) {
                    continue;
                }

                var mgIndex = masterGrid.length;
                masterGrid[mgIndex] = grids[i][0][j].slice(0, -1);
                for (var k = 1; k < numGridsSquared; k++) {
                    // dont add the last bit of each strip because the borders of each edge are the same
                    var stripToAdd = (k != numGridsSquared - 1) ? grids[i][k][j].slice(0, -1) : grids[i][k][j];
                    masterGrid[mgIndex] = masterGrid[mgIndex].concat(stripToAdd);
                }
            }
        }
    }

    var after = Date.now();
    console.log("Time to create Master Grid:" + (after - before));

    // prepare data for use by Three.js
    before = Date.now();
    var geometry = prepareData(masterGrid);
    after = Date.now();
    console.log("Time to prepare geometry:" + (after - before));

    

    var mesh = new THREE.Mesh(geometry, material);

    var scale = 1 / masterGrid.length;
    mesh.scale.set(scale, scale, scale);

    return mesh;

    // // render!
    // before = Date.now();
    // // createScene(geometry, masterGrid.length);
    // after = Date.now();
    // console.log("Time to render:" + (after - before));

    // var mainAfter = Date.now();
    // console.log("Time to run main:" + (mainAfter - mainBefore));
}


