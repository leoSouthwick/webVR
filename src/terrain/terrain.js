import * as THREE from 'three';
import { randomNormal } from './randomNormal.js';

function Coordinate(x, y) {
    this.x = x;
    this.y = y;
}

// print heights to console [useless for large grids]
function gridToConsole(grid) {
    var str = "";
    var gridSize = grid.length;

    for (var i = 0; i < gridSize; i++) {
        for (var k = 0; k < gridSize; k++) {
            str += Math.round(grid[i][k]) + "\t";
        }

        str = "";
    }
}

/* Average grid heights and add a randomly distributed normal centered at 0 */
function variedAverage(grid, variation_scale, values) {
    var sum = 0.0;

    for (var i = 0; i < values.length; i++) {
        sum += grid[values[i].x][values[i].y];
    }

    var random = randomNormal(0, .33);
    var avg = sum / values.length;

    /* create scaling factor height_scale to make high peaks more rough and beaches and the ocean more smooth
       basically more height = more rough
       height_level is between 0 and 1      */
    var height_level = (avg + grid.length / 2) / grid.length;

    var height_scale;
    if (height_level > .8) {
        height_scale = 1;
    } else {
        /* 
         * scaling equation: https://docs.google.com/a/iastate.edu/spreadsheets/d/1Ra9ffuAv2Db9lx1uYCne1lHRfD4ETJUAm6id9wkY7jI/edit?usp=sharing
         * height_scale = 3.366*height_level^3 - 1.952*height_level^2 + 0.353*height_level + 0.248 
         */
        height_scale = (((3.366 * height_level) - 1.952) * height_level + 0.353) * height_level + 0.248;
    }
    return avg + (random * variation_scale * height_scale);
}

/* Recursively generate height values for 2D terrain array

    Grid side length must be of the sequence 2, 3, 5, 9, 17, 33, 65 ...
    otherwise the recursion will fail at some point
    A(n+1) =  [2 * A(n)] - 1

-----------------------------------------------------------------------------------------
    O = 4 known corner points
    0 = 4 new edge points and one new center point to assign

    half_length         [ start.x , (start.y + sideLength) ]
        |                 |
        v                 v
    
        -    -            O     0     O   <-- [ (start.x + sideLength) , (start.y + sideLength) ]
        |    |
        -    |            0     0     0
             |
             -            O     0     O   <-- [ (start.x + sideLength) , start.y ]

             ^            ^
             |            |
     side_length      [ start.x, start.y ] <-- bottom_left
-----------------------------------------------------------------------------------------
 The depth first algorithm created a lot of sharp edges:   
 prevent sharp edges by defining heights bredth first */
function BFHeights(start, grid_length, scale, grid, steps, H) {
    var half_length = side_length / 2;

    for (var i = 0; i < steps; i++) {
        scale *= Math.pow(0.5, H / 2);
        var side_lengths_at_level = Math.pow(2, i);
        var side_length = grid_length / side_lengths_at_level;
        half_length = side_length / 2;

        for (var k = 0; k < side_lengths_at_level; k++) {
            for (var j = 0; j < side_lengths_at_level; j++) {
                // get the 4 points bounding this chunk
                var bottom_left  = new Coordinate(start.x + (side_length * k), start.y + (side_length * j));
                var top_left     = new Coordinate(bottom_left.x              , bottom_left.y + side_length);
                var top_right    = new Coordinate(bottom_left.x + side_length, bottom_left.y + side_length);
                var bottom_right = new Coordinate(bottom_left.x + side_length, bottom_left.y              );

                // get the center points of all the sides of this chunk and the exact center point itself
                var left_edge   = new Coordinate(bottom_left.x              , bottom_left.y + half_length);
                var right_edge  = new Coordinate(bottom_left.x + side_length, bottom_left.y + half_length);
                var bottom_edge = new Coordinate(bottom_left.x + half_length, bottom_left.y              );
                var top_edge    = new Coordinate(bottom_left.x + half_length, bottom_left.y + side_length);
                var center      = new Coordinate(bottom_left.x + half_length, bottom_left.y + half_length);

                // Assign values to all side centers and the actual center
                // Do not assign values if a value already exists, which happens when this is a border with a neighbor grid
                grid[left_edge.x  ][left_edge.y  ] = grid[left_edge.x  ][left_edge.y  ] || variedAverage(grid, scale, [bottom_left , top_left    ]);
                grid[right_edge.x ][right_edge.y ] = grid[right_edge.x ][right_edge.y ] || variedAverage(grid, scale, [bottom_right, top_right   ]);
                grid[top_edge.x   ][top_edge.y   ] = grid[top_edge.x   ][top_edge.y   ] || variedAverage(grid, scale, [top_left    , top_right   ]);
                grid[bottom_edge.x][bottom_edge.y] = grid[bottom_edge.x][bottom_edge.y] || variedAverage(grid, scale, [bottom_left , bottom_right]);
                grid[center.x     ][center.y     ] = grid[center.x     ][center.y     ] || variedAverage(grid, scale, [bottom_left , top_left    , top_right, bottom_right]);
            }
        }
    }
}

/* gridSize = 2 * prevGridSize - 1 requires a loop calculating every grid size
   gridSize = 2^(i+1) - (2^i - 1) is equivalent but does not require loop      */
export function calcGridSize(steps) {
    var base = Math.pow(2, steps);
    return 2 * base - (base - 1);
}

/* create a 2D array with a given side length on both sides */
function createDoubleArr(gridSize) {
    var grid = new Array(gridSize);

    for (var i = 0; i < gridSize; i++) {
        grid[i] = new Array(gridSize)
    }
    return grid;
}

/* There are two ways to create a new grid, 
   (1) by passing a neighbors object which can have north, east, south, and west neighbors properties.
        each neighbor will be accounted for so that the edge of this grid matches the bordering edge of that grid.
   (2) by passing in a preGrid that defines the grid up to a certain step level
        this grid is of the sequence explained above 2x2, 3x3, 5x5, 9x9 ... etc.  */
export function createGrid(steps, gridSize, neighbors, preGrid, H) {
    var start = new Coordinate(0, 0);
    var grid = createDoubleArr(gridSize);

    if (!preGrid) {
        /* enter here if there was a preGrid defined */
        if (neighbors) {
            // if the east neighbor exists, make the east border match the neighbor's west
            if (neighbors.east) {
                grid[gridSize - 1] = neighbors.east[0];
            }

            // if the west neighbor exists, make the west border match the neighbor's east
            if (neighbors.west) {
                grid[0] = neighbors.west[gridSize - 1];
            }

            // if the north neighbor exists, make the north border match the neighbor's south
            if (neighbors.north) {
                for (var i = 0; i < gridSize; i++) {
                    grid[i][gridSize - 1] = neighbors.north[i][0];
                }
            }

            // if the south neighbor exists, make the south border match the neighbor's north
            if (neighbors.south) {
                for (var i = 0; i < gridSize; i++) {
                    grid[i][0] = neighbors.south[i][gridSize - 1];
                }
            }
        }

        // set the corners that are still not set yet
        // Assign inital corner values if they don't already exist. Should be randomly generated
        grid[0           ][0           ] = grid[0           ][0           ] || randomNormal(10, gridSize / 4);
        grid[0           ][gridSize - 1] = grid[0           ][gridSize - 1] || randomNormal(10, gridSize / 4);
        grid[gridSize - 1][0           ] = grid[gridSize - 1][0           ] || randomNormal(10, gridSize / 4);
        grid[gridSize - 1][gridSize - 1] = grid[gridSize - 1][gridSize - 1] || randomNormal(10, gridSize / 4);

    } else {
        /* enter here if there was a preGrid defined */
        var scale = (gridSize - 1) / (preGrid.length - 1);
        for (var i = 0; i < preGrid.length; i++) {
            for (var j = 0; j < preGrid.length; j++) {
                grid[Math.floor(i * scale)][Math.floor(j * scale)] = preGrid[i][j];
            }
        }
    }

    // generate heights and track the time it takes
    var before = Date.now();
    BFHeights(start, (gridSize - 1), gridSize / 2, grid, steps, H);
    var after = Date.now();
    console.log("Time to generate heights:" + (after - before));

    return grid;
}

export function prepareData(grid) {

    var gridSize = grid.length;

    /* create a geometry object to hold all vertices */
    var geometry = new THREE.Geometry();

    /* track the number of verices to easily create faces */
    var numVertices = 0;
    var before = Date.now();

    var index = 0;
    /* add vertices and faces for each square in the grid */
    for (var x = 0, xplus = 1; x < gridSize - 1; x = xplus, xplus++) {
        for (var z = 0, zplus = 1; z < gridSize - 1; z = zplus, zplus++) {
            // vertices
            var topRight = null;
            var topLeft  = null;
            var botRight = null;
            var botLeft  = null;

            // colors
            var topRightColor = null;
            var topLeftColor  = null;
            var botRightColor = null;
            var botLeftColor  = null;

            /*
             * Values are stored as chunks into the array like so:
             * [TopRight, BotRight, BotLeft, TopLeft, TopRight, BotLeft]
             *
             * To get already created vertices we can use this to our advantage.
             */
            if (z > 0) {
                // reuse what was just put in the array
                var botLeftInd = index - 3; // lower square's top left
                var botRightInd = index - 2; // lower square's top right
                botLeft       = geometry.vertices[botLeftInd];
                botLeftColor  = geometry.colors[botLeftInd];
                botRight      = geometry.vertices[botRightInd];
                botRightColor = geometry.colors[botRightInd];
            }
            if (x > 0) {
                // reuse what was put into the array one full gridSize back
                var topLeftInd = index - (6 * (gridSize - 1)); // left square's top right
                var botLeftInd = topLeftInd + 1; // left square's bottom right
                botLeft      = botLeft      || geometry.vertices[botLeftInd];
                botLeftColor = botLeftColor || geometry.colors[botLeftInd];
                topLeft      = geometry.vertices[topLeftInd];
                topLeftColor = geometry.colors[topLeftInd];
            }

            // get heights
            var topRightHeight = grid[xplus][zplus];
            var topLeftHeight  = grid[x    ][zplus];
            var botRightHeight = grid[xplus][z    ];
            var botLeftHeight  = grid[x    ][z    ];

            // these values always must be created each iteration
            topRight      = new THREE.Vector3(xplus, topRightHeight, zplus);
            topRightColor = getColor(topRightHeight);

            // these values must be created if they weren't before, use short circuit style
            topLeft      = topLeft      || new THREE.Vector3(x, topLeftHeight, zplus);
            topLeftColor = topLeftColor || getColor(topLeftHeight);

            botRight      = botRight      || new THREE.Vector3(xplus, botRightHeight, z);
            botRightColor = botRightColor || getColor(botRightHeight);

            botLeft      = botLeft      || new THREE.Vector3(x, botLeftHeight, z);
            botLeftColor = botLeftColor || getColor(botLeftHeight);

            /* add vertices for the lower-right triangle */
            geometry.vertices[index] = topRight;
            geometry.colors[index++] = topRightColor;

            geometry.vertices[index] = botRight;
            geometry.colors[index++] = botRightColor;

            geometry.vertices[index] = botLeft;
            geometry.colors[index++] = botLeftColor;

            /* add vertices for the upper-left triangle [pretty sure we need CCW coordinate order] */
            geometry.vertices[index] = topLeft;
            geometry.colors[index++] = topLeftColor;

            geometry.vertices[index] = topRight;
            geometry.colors[index++] = topRightColor;

            geometry.vertices[index] = botLeft;
            geometry.colors[index++] = botLeftColor;

            /* push face for lower-right triangle */
            var face1 = new THREE.Face3(numVertices, numVertices + 1, numVertices + 2);
            face1.vertexColors.push(topRightColor);
            face1.vertexColors.push(botRightColor);
            face1.vertexColors.push(botLeftColor);
            geometry.faces.push(face1);

            /* push face for upper-left triangle */
            var face2 = new THREE.Face3(numVertices + 3, numVertices + 4, numVertices + 5);
            face2.vertexColors.push(topLeftColor);
            face2.vertexColors.push(topRightColor);
            face2.vertexColors.push(botLeftColor);
            geometry.faces.push(face2);

            /* update the number of vertices that we have added once faces have been created */
            numVertices += 6;
        }
    }

    var after = Date.now();
    console.log("Time to create vertices:" + (after - before));

    /* remove duplicate vertices and update faces.  Better performance? */
    before = Date.now();
    // geometry.mergeVertices(geometry);
    after = Date.now();
    console.log("Time to merge vertices:" + (after - before));

    /* compute face normals so we can do lighting */
    before = Date.now();
    geometry.computeFaceNormals();
    after = Date.now();
    console.log("Time to compute face normals:" + (after - before));

    return geometry;
}

/* COLORS */

// only have one copy of each color for speed and memory optimization
var WATER_COLOR = new THREE.Color(0x129793); // water color
var SAND_COLOR = new THREE.Color(0xc2b280);  // sand color
var GRASS_COLOR = new THREE.Color(0x007b0c); // grass color
var STONE_COLOR = new THREE.Color(0x444250); // stone color
var SNOW_COLOR = new THREE.Color(0xffffff);
var LIGHT_STONE_COLOR = new THREE.Color(0x8B4513);
var WATER_COLORS; // array of water colors

/**
 * Given a depth value (0 to -70, will be truncated if it is out of this range),
 * this function returns the appropriate water THREE.Color object corresponding
 * to this depth.
 */
function getWaterColor(depth) {
    // if Water colors array isn't created yet, then create it
    if (!WATER_COLORS) {
        WATER_COLORS = [];
        var HSL = WATER_COLOR.getHSL();
        for (var i = 0; i < 20; i++) {
            var color = new THREE.Color()
            color.copy(WATER_COLOR);
            color.offsetHSL(0, 0, -i * HSL.l * 0.1); //decrease the lightness by 10% per step
            WATER_COLORS[WATER_COLORS.length] = color;
        }
    }
    // calculate an index to use for the Water_Colors array [70 is max depth considered for color change] 19 number of colors in array
    var index = Math.floor((Math.min(-depth, 70) / 70) * 19);
    return WATER_COLORS[index];
}

/**
 * Gets a THREE.Color object for this height value.
 * Height can be -70 to 70 and will be truncated if
 * it exceeds these values.
 */
function getColor(height) {
    // randomly scale height by a factor normally distributed about 1 to allow for variation
    var rand = Math.abs(randomNormal(1, .25)) * height;

    // return a color to use
    if (rand < 0) {
        return getWaterColor(height);
    } else if (rand < 2) {
        return SAND_COLOR;
    } else if (rand < 6) {
        return LIGHT_STONE_COLOR;
    }else if (rand < 70) {
        return STONE_COLOR;
    }
    return SNOW_COLOR;
}