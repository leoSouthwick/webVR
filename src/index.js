//---------------
// Imports
//---------------
import {Scene, PerspectiveCamera, WebGLRenderer, BoxBufferGeometry, EdgesGeometry, MeshBasicMaterial, Mesh} from 'three';

// I wan't to figure out how to import webVR Polyfill here as well.

// Wrap non-modules that use THREE.js as a global container
import * as THREE from 'three';
import OrbitControls from '../node_modules/three/examples/js/controls/OrbitControls.js'
import VRControls from '../node_modules/three/examples/js/controls/VRControls.js'
import VREffect from '../node_modules/three/examples/js/effects/VREffect.js'
OrbitControls(THREE);
VRControls(THREE);
VREffect(THREE);

// Custom modules
import terrainMesh from './terrain/main.js';


//---------------
// Load
//---------------

// Textures
var loader = new THREE.TextureLoader();
loader.load('../content/box.png', onTextureLoaded);


//---------------
// Start Scene
//---------------

// Setup three.js WebGL renderer. Note: Antialiasing is a big performance hit.
var renderer = new THREE.WebGLRenderer({antialias: false});
renderer.setPixelRatio(Math.floor(window.devicePixelRatio));

// Append the canvas element created by the renderer to document body element.
document.body.appendChild(renderer.domElement);

var scene = new Scene();

var camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

// Apply VR controls
var controls = new THREE.VRControls(camera);

// Apply stereo rendering
var effect = new THREE.VREffect(renderer);
effect.setSize(window.innerWidth, window.innerHeight);


//--------------------
// Add Items to Scene
//--------------------

// Objects
//--------

// Add terrain to the scene
var material = new THREE.MeshStandardMaterial({
    vertexColors: THREE.VertexColors,
    wireframe: false,
    metalness: 0,
    roughness: 1
});
var terrain = terrainMesh(material);
terrain.position.set(-0.5, -0.75, -1.5);
scene.add(terrain);

// Lighting
//---------
var light = new THREE.PointLight(0x666666, 5)
light.position.set(0,0,1);
scene.add(light);

// Sun
var sun = new THREE.DirectionalLight(0x666666, 1.5);
sun.position.set(-0.4, 1, -0.2);
scene.add(sun);

// Ambient
var ambient = new THREE.AmbientLight(0x222222);
scene.add(ambient);


// Skybox
//-------
var boxWidth = 30;

function onTextureLoaded(texture) {
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(boxWidth, boxWidth);

    var geometry = new THREE.BoxGeometry(boxWidth, boxWidth, boxWidth);
    var material = new THREE.MeshBasicMaterial({
        map: texture,
        color: 0xffffff,
        side: THREE.BackSide
    });
    var skybox = new THREE.Mesh(geometry, material);
    console.log("adding");
    scene.add(skybox);
}


//---------------
// Render loop.
//---------------

function animate(){

    // Update VR headset position and apply to camera.
    controls.update();
    
    //camera.zoom = zoom;
    camera.updateProjectionMatrix();

    // Render the scene.
    effect.render(scene, camera);

    // Object movement
    animateObjects();

    // Keep looping.
    vrDisplay.requestAnimationFrame(animate);
}

function animateObjects() {
    terrain.translateX(0.5);
    terrain.translateZ(0.5);
    terrain.rotateY(0.005);
    terrain.translateX(-0.5);
    terrain.translateZ(-0.5);
}





//-------------------
// Behind the Scenes
//-------------------


//Get the VRDisplay and start animation loop
var vrDisplay = null;
navigator.getVRDisplays().then(function(displays) {
    if (displays.length > 0) {
        vrDisplay = displays[0];
        // Kick off the render loop.
        vrDisplay.requestAnimationFrame(animate);
    }
});

function onResize() {
    console.log('Resizing to %s x %s.', window.innerWidth, window.innerHeight);
    effect.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
}

function onVRDisplayPresentChange() {
    console.log('onVRDisplayPresentChange');
    onResize();
}

// Resize the WebGL canvas when we resize and also when we change modes.
window.addEventListener('resize', onResize);
window.addEventListener('vrdisplaypresentchange', onVRDisplayPresentChange);


//-------------------
// Event Handlers.
//-------------------

//Handle enter VR Mode (button click)
document.querySelector('button#vr').addEventListener('click', function() {
    vrDisplay.requestPresent([{source: renderer.domElement}]);
});

document.querySelector('button#reset').addEventListener('click', function() {
    vrDisplay.resetPose();
});

document.querySelector('button#fullscreen').addEventListener('click', function() {
    enterFullscreen(renderer.domElement);
});


function enterFullscreen (el) {
    if (el.requestFullscreen) {
        el.requestFullscreen();
    } else if (el.mozRequestFullScreen) {
        el.mozRequestFullScreen();
    } else if (el.webkitRequestFullscreen) {
        el.webkitRequestFullscreen();
    } else if (el.msRequestFullscreen) {
        el.msRequestFullscreen();
    }
}