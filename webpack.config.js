const path = require('path');

module.exports = {

    // Define entry point
    entry: './src/index.js',

    // Un-comment to have WebPack build on file save
    //watch: true,    

    // Define custom locations for webpack to look for modules
    resolve: {
        alias: {
            'three/OrbitControls': path.resolve(__dirname,  'node_modules/three/examples/js/controls/OrbitControls.js')
        }
    },

    // Info on where to store output
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'www')
    },

    // Using three.js with webpack https://github.com/mrdoob/three.js/issues/10328
    module: {
        rules: [
            {
                loader: 'babel-loader',
                query: {
                    compact: true
                }
            }
        ]
    }
};