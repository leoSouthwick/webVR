import * as THREE from 'three';

module.exports = function(){

    // Constructor
    //------------
    function Terrain(iterations) {

        this.iterations = iterations;
        this.currentIteration = 0;

        this.segments = Math.pow(2, iterations);

        this.geometry = createGeometry(this.segments);
    }

    // Public Functions
    //-----------------
    Terrain.prototype = {
        getNextAnimationMatrix = function(){ 
        
        }
    }

    // Define the number of iterations to compute. Sidelength = 2^(iterations) + 1
    const iterations = 3; 


    function createGeometry(segments){
        return THREE.PlaneGeometry( segments, segments, segments, segments )
    }

}